<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <tabs>standard-Chatter</tabs>
    <tabs>standard-Campaign</tabs>
    <tabs>standard-Lead</tabs>
    <tabs>standard-Contact</tabs>
    <tabs>standard-Opportunity</tabs>
    <tabs>standard-report</tabs>
    <tabs>standard-Dashboard</tabs>
    <tabs>Fluxo</tabs>
    <tabs>standard-Quote</tabs>
    <tabs>Log_de_integracao__c</tabs>
    <tabs>Faturamento__c</tabs>
    <tabs>Emplacamentos__c</tabs>
    <tabs>Share__c</tabs>
    <tabs>Item_Solicitacao__c</tabs>
    <tabs>Sugestao_futura__c</tabs>
    <tabs>version__c</tabs>
    <tabs>Coletas__c</tabs>
    <tabs>Expense__c</tabs>
</CustomApplication>
