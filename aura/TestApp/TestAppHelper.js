({
    handleAjaxRequest : function(component, event, helper) {
        const xhr = new XMLHttpRequest();

        const number = component.get('v.number');

        const url = 'http://api.icndb.com/jokes/random/' + number;

        xhr.open('GET', url, true);

        xhr.onload = function() {
            if(this.status === 200) {
                const response = JSON.parse(this.responseText);

                let output = '<ul class=\'slds-list--dotted\'>';

                if(response.type === 'success') {
                    response.value.forEach(function(joke){
                        output += `<li>${joke.joke}</li>`;
                    });
                }

                output += '<ul>';
                component.set('v.html', output);
            }
        };

        xhr.send();
    }
})