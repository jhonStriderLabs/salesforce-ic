public class AccountDemonstrationControllerExt {

    public Account account {get; set;}
    public account atualizaCliente {get; set;}
    public account consultaCliente {get; set;}
        
    public string paramCNPJ {get;set;}    

    public boolean insertButton {get;set;}
    public boolean updateButton {get;set;}
    public boolean searchButton {get;set;}
    
    public boolean formInsert {get;set;}
    public boolean formUpdate {get;set;}
    public boolean formSearch {get;set;}
    
    public AccountDemonstrationControllerExt(ApexPages.StandardController controller){
        account = (account)controller.getRecord();
        insertButton = true;
		updateButton = true;     
    	searchButton = true;
    }
     
	public void registrarCliente(){
        insertObject(account);
		insertButton = true;
      	formInsert  = false;
    }
    
  	public void formInsert(){
      	 insertButton = false;
     	 formInsert  = true;
   	}

    
    public void formSearch(){
      	 searchButton = false;
     	 formSearch  = true;
   	}


	public void atualizarDados(){
    	updateObject(atualizaCliente);  
	}
    public void formUpdate(){
      	 updateButton = false;
     	 formUpdate  = true;
   	}
    
    
    public void getClienteUpdate(){
   	    atualizaCliente = searchObject(paramCNPJ);
    }
    public void getDadosCliente(){
   	    consultaCliente = searchObject(paramCNPJ);
    }
    
    public account searchObject(String CNPJ){
     
        account[] returnAccount = [select id, name, CNPJ__c,Phone, type, Site, SicDesc from Account where CNPJ__c =: CNPJ];
        
        if (!returnAccount.isEmpty()){
            paramCNPJ = null; 
            return returnAccount[0];
         }else{ 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,
            'Registro não encontrado na base'));
            return null;
           }
    }
    
    public void updateObject(sObject entitie){
    
    Database.SaveResult LogUpdate = Database.update(entitie, false);
    
    if (LogUpdate.isSuccess()) {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,
            'Registo Atualizado com Sucesso!'));
        	entitie.clear();
      }else{ 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
            'Erro ao Criar o registro'));                
          }          
    }
    public void insertObject(sObject entitie){
        
   	Database.SaveResult LogInsert = Database.insert(entitie, false);
    if (LogInsert.isSuccess()) {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,
            'Registo Criado com Sucesso!'));
        	 entitie.clear();
    }else{ 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
            'Erro ao Criar o registro'));                
           }          
    }
    
    
}