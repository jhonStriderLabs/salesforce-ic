public class AnaliseSintomasChatBot {

    public class doencaSaida{
        @InvocableVariable(required= true)
        public String doenca ;
    }
    
    public class doencaEntrada{
        @InvocableVariable(required= true)
        public String doenca ;
    }
   
    @InvocableMethod(Label ='Trazer Lista de Doenças')
    public static List<doencaSaida> getDoencas()
    {		
         List<doencaSaida> lstOutput = new List<doencaSaida>();
         doencaSaida d1 = new doencaSaida();
		 d1.doenca = 'Dor de Cabeça';
         lstOutput.add(d1);

         doencaSaida d2 = new doencaSaida();
         d2.doenca = 'Dor de Barriga';
         lstOutput.add(d2);
        
         doencaSaida d3 = new doencaSaida();
         d3.doenca = 'Contração';
         lstOutput.add(d3);
        
         return  lstOutput;     
    }
    
}