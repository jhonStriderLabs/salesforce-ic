public class AnimalLocator {
        
    public static String getAnimalNameById(integer id){
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://th-apex-http-callout.herokuapp.com/animals/' + id);
        request.setMethod('GET');
        
        String strResp ='';
        
        HttpResponse response = http.send(request);
                
        system.debug('Response' +response.getStatusCode());
        system.debug('Response' +response.getBody());
        
        if(response.getStatusCode() ==200)
        {
            Map<String,Object> results = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
            Map<String,object> animals = (map<string, Object>)results.get('animal');
            
            system.debug('Received the following animals' + animals);
            
            strResp = string.valueOf(animals.get('name'));
            system.debug('strResp >>>>>' + strResp);
        }
        return strResp;
    }

}