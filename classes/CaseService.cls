public with sharing class CaseService {

    public static void closeCases(Set<id> casesIds, String closeReason){

        List<Case> listObj = new List<Case>();
        for(id caseUpdate: casesIds){
            listObj.add(new case(id = caseUpdate, reason = closeReason, status = 'Closed'));
        }
        update listObj;
    }

}