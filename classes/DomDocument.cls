public class DomDocument {

    // Pass in the URL for the request
    // For the purposes of this sample,assume that the URL
    // returns the XML shown above in the response body
    public void parseResponseDom(String url){

       string xml = '<OrderDetails><OrderDetailID>2584</OrderDetailID><GiftWrapCost>0.0000</GiftWrapCost><GiftWrapNote/><ProductCode>800006</ProductCode></OrderDetails>';
        Dom.Document doc = new Dom.Document();
       doc.load(xml);

        //Retrieve the root element for this document.
        Dom.XMLNode ordDtls = doc.getRootElement();

        String OrderDetailId= ordDtls.getChildElement('OrderDetailId', null).getText();
        String prdCode = ordDtls.getChildElement('ProductCode', null).getText();
        // print out specific elements
        System.debug('OrderDtlID: ' + OrderDetailId);
        System.debug('Prod Code: ' + prdCode );

        // Alternatively, loop through the child elements.
        // This prints out all the elements of the address
        for(Dom.XMLNode child : ordDtls.getChildElements()) {
           System.debug(child.getText());
        }
    }
}