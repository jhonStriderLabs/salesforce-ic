public class GoogleServiceV3 {
    
    public static final String JWT_AUD = 'https://www.googleapis.com/oauth2/v4/token';
    public static final String JWT_GRANT_TYPE = 'urn:ietf:params:oauth:grant-type:jwt-bearer';
    public static final String JWT_ENDPOINT = 'https://www.googleapis.com/oauth2/v4/token';
    public static String keyServer = '';
    
    //private SignServer signServer;
    
    private String token_type;
    private String access_token;
    
    public String getAccessToken(){
        return this.access_token;
    }
    
    public GoogleServiceV3() {
    }
    
    //public GoogleServiceV3(SignServer signServer) {
    //    this.signServer = signServer;
    //}
    
    public blob getSignature(){
    	blob newPrivateKeyBlob = EncodingUtil.base64Decode( keyServer );
    	
    	return newPrivateKeyBlob;
    }
    /*
    public Boolean authenticate(JWT jwt) {
        if (jwt.aud == null) {
            jwt.aud = JWT_AUD;
        }
        if (jwt.iat == null) {
            jwt.iat = Datetime.now();
        }
        if (jwt.exp == null) {
            jwt.exp = jwt.iat.addHours(1);
        }
        
        String str = jwt.getEncodedString();
        if (this.signServer != null) {
        	system.debug( 'PRIVATE KEY= '+ keyServer);
        	system.debug('STRING DE CONEXAO ANTES= '+str);
        	if( !Test.isRunningTest() ) str += '.' + jwt.sign(getSignature());
        	system.debug('STRING DE CONEXAO= '+str);
        	//ESSE TRECHO COMENTADO É O ORIGINAL
            //str += '.' + signServer.sign(str);
        } else {
            throw new GoogleServiceException('Can not sign to jwt');
        }
        System.debug('JWT:' + jwt);
        HttpRequest req = createRequest();
        String content = 'grant_type=' + EncodingUtil.urlEncode(JWT_GRANT_TYPE, 'utf-8') +
            '&assertion=' + EncodingUtil.urlEncode(str, 'utf-8');
            system.debug('CHAVE JWT ASSINADA= '+EncodingUtil.urlEncode(str, 'utf-8'));
        req.setEndpoint(JWT_ENDPOINT);
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        req.setMethod('POST');
        req.setBody(content);
        
        HttpResponse res;
        if( !Test.isRunningTest() ) res = sendRequest(req);
        else {
        	CalendarConnection conn = new CalendarConnection();
        	res = new Httpresponse();
        	res.setBody( conn.getReturnResponseAuthentication() );
        	res.setStatusCode(200);
        	res.setStatus('Success');
        }
        if (isOK(res)) {
        	System.debug('RETORNO GOOGLE= '+res);
            GoogleOAuth.AuthResponse auth = (GoogleOAuth.AuthResponse)JSON.deserialize(res.getBody(), GoogleOAuth.AuthResponse.class);
            this.token_type = auth.token_type;
            this.access_token = auth.access_token;
            return  true;
        }
        throw buildResponseException(res);
    }*/
    
    public void setAccessToken(String tokenType, String token) {
        this.token_type = tokenType;
        this.access_token = token;
    }
    
  /*  public String get(String url) {
        return get(url, null);
    }*/
    
  /*  public String get(String url, Map<String, String> params) {
        Boolean first = true;
        if (params != null && params.size() > 0) {
            for (String key : params.keySet()) {
                url += (first ? '?' : '&');
                url += EncodingUtil.urlEncode(key, 'utf-8') + '=' + EncodingUtil.urlEncode(params.get(key), 'utf-8');
                first = false;
            }
        }
        HttpRequest req = createRequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        HttpResponse res;
        if(!Test.isRunningTest() ) res = sendRequest(req);
        else{
        	CalendarConnection conn = new CalendarConnection();
        	res = new Httpresponse();
        	res.setStatus('Success');
        	res.setStatusCode(200);
        	if( url.contains('/calendars/') && url.contains('/events/')) res.setBody( conn.getReturnResponseEvent() );
        }
        
        if (isOK(res)) {
        	system.debug('BODY=CALENDAR=res.getBody()= '+res.getBody());
            return res.getBody();
        }
        throw buildResponseException(res);
    }*/
    
   /* public String post(String url, String jsonStr) {
        HttpRequest req = createRequest();
        req.setEndpoint(url);
        req.setMethod('POST');
        req.setHeader('Content-Type','application/json');
        req.setBody(jsonStr);
        
        HttpResponse res = sendRequest(req);
        system.debug( 'CR=res= ' +res );
        system.debug( 'CR=res=getBody= ' +res.getBody() );
        if (isOK(res)) {
            return res.getBody();
        }
        throw buildResponseException(res);
    }*/
    
  /*  public String patch(String url, String jsonStr) {
        HttpRequest req = createRequest();
        req.setEndpoint(url);
        req.setMethod('POST');
        req.setHeader('X-HTTP-Method-Override', 'PATCH');
        req.setHeader('Content-Type','application/json');
        req.setBody(jsonStr);
        HttpResponse res;
        if(!Test.isRunningTest()) res = sendRequest(req);
        else {
        	CalendarConnection conn = new CalendarConnection();
        	res = new Httpresponse();
        	res.setStatusCode(200);
        	res.setStatus('Success');
        	res.setBody( conn.getReturnResponseCalendar() );
        }
        if (isOK(res)) {
            return res.getBody();
        }
        throw buildResponseException(res);
    }*/
    
    /*------------------------------------------------------------
    Company:       Accenture/SulAmerica
    Description:   Atualiza um evento no google calendar
    Inputs:        <EVENT_SYNC__c compromisso> - Objeto
                   temporario do SFDC que armazena todas as 
                   informações do compromisso para integrar com 
                   o google calendar. 
    Returns:       NONE 
    Exception:     NONE
    History
    23/12/2015     Carlos Carvalho      Criação do método
    ------------------------------------------------------------*/
   /* public String put(String url, String jsonStr) {
        HttpRequest req = createRequest();
        req.setEndpoint(url);
        req.setMethod('PUT');
        req.setHeader('Content-Type','application/json');
        req.setBody(jsonStr);
        HttpResponse res;
        if( !Test.isRunningTest() ) res = sendRequest(req);
        else{
        	res = new Httpresponse();
        	res.setStatus('Success');
        	res.setStatusCode(200);
        	CalendarConnection con = new CalendarConnection();
        	res.setBody( con.getReturnResponseEvent() );
        }
        if (isOK(res)) {
            return res.getBody();
        }
        throw buildResponseException(res);
    }*/
    
   /* public String doDelete(String url) {
        HttpRequest req = createRequest();
        req.setEndpoint(url);
        req.setMethod('DELETE');
        HttpResponse res = sendRequest(req);
        if (isOK(res)) {
            return res.getBody();
        }
        throw buildResponseException(res);
    }*/
    
    private boolean isOK(HttpResponse res) {
    	system.debug('res.getStatusCode()= '+res.getStatusCode());
        return res.getStatusCode() >= 200 && res.getStatusCode() < 300;
    }
    
    private HttpRequest createRequest() {
        HttpRequest req = new HttpRequest();
        req.setHeader('User-Agent','apex-google-api');
        if(this.access_token != null && this.token_type != null) {
            String str = this.token_type + ' ' + this.access_token;
            req.setHeader('Authorization', str);
        }
        return req;
    }
    
    /*private HttpResponse sendRequest(HttpRequest req) {
        Http http = new Http();
        HttpResponse res;
        try {
            if( !Test.isRunningTest() ) res = http.send(req);
            else {
            	res = new Httpresponse();
            	res.setStatusCode(200);
            	res.setStatus('OK');
            }
            System.debug('STATUS:' + res.getStatusCode());
        } catch( System.Exception e) {
        	system.debug('ENTROU NESSE EXCEPTION');
        }
        List< String > emails = new List< String >();
        for( User u : [ Select Id, Email From User Where Id =: Userinfo.getUserId() ]){
            emails.add( u.Email );
        }
        
      Utils lUtil = new Utils();
        if(res.getStatusCode() == 403 ) lUtil.enviarEmail( Label.CRM_4_GOOGLE_CALENDAR_MSG_ERRO_403, 
                                          Label.CRM_6_GOOGLE_CALENDAR_TXT_ERRO_403, emails );
        else if( res.getStatusCode() == 404 ) lUtil.enviarEmail( Label.CRM_5_GOOGLE_CALENDAR_MSG_ERRO_404, 
                                                Label.CRM_7_GOOGLE_CALENDAR_TXT_ERRO_404, emails );
        return res;
    }*/
    
    private GoogleServiceException buildResponseException(HttpResponse res) {
        String msg = 'STATUS: ' + res.getStatus() + 
            '\nSTATUS_CODE: ' + res.getStatusCode() +
            '\nBODY: ' + res.getBody();
        System.debug(msg);
        return new GoogleServiceException(msg);
    }
    
}