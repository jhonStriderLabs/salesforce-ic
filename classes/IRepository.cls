global interface IRepository {
	
	SObject get(Id id) ;
	SObject getCustomFieldsById(String id, List<String> selectFields);
	SObject getCustomFieldsById(String id, List<String> selectFields, Boolean isForView);
	void deleteObject(List<SObject> entities);
	void insertObject(List<SObject> entities);
	void updateObject(List<SObject> entities);
	void saveObject(List<SObject> entities);

	List<Sobject> queryObjects(Repository.QueryBuilder queryBuilder);
	Integer queryCount(Repository.QueryBuilder queryBuilder);
	List<SObject> queryObjects(Specification specification);
}