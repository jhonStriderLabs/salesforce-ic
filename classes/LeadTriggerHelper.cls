public class LeadTriggerHelper {
    
    public static void createTasks(List<Lead> listLead){
       	List<Task> listTasks = new List<Task>();

        for(Lead leadLoop : listLead){
               
           if(leadLoop.FormasDeContato__c != null)
                    
              for(String formaContato: leadLoop.FormasDeContato__c.split(';')){
                   if(formaContato == 'SMS' || formaContato == 'Whatsapp')
                   listTasks.add(new Task(WhoId = leadLoop.Id, Subject = formaContato, Status ='Não Iniciado', Priority = 'Normal'));
              }
          	
         }	
        
        if(listTasks.size() > 0){
            try{
                insert listTasks;
            }catch(DMLException e){
                System.Debug(e.getStackTraceString());
            }
        }
     }
}