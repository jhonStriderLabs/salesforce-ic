public class MaintenanceRequestHelper {
    
    public static void updateWorkOrders(Map<id, Case> oldCaseMap, Map<id, Case> newCaseMap){

        List<Case> listInsertCase = new List<Case>();
        Map<id, Case> caseMap = new Map<id,Case>();
        List<Work_Part__c> allWorkParts = new List<Work_Part__c>();
        
  		for(Case c: [SELECT   ID,Equipment__c,Vehicle__c, (SELECT ID, Quantity__c, Equipment__r.Name, 
                                 Equipment__r.Cost__c, Equipment__r.Current_Inventory__c,
                                 Equipment__r.Lifespan_Months__c, Equipment__r.Maintenance_Cycle__c,
                                 Equipment__r.Replacement_Part__c,Equipment__r.Warehouse_SKU__c 
                                 FROM Work_Parts__r) FROM Case where id IN:newCaseMap.KeySet() 
                     			 AND (Status='Concluído' OR Status = 'Closed') 
                     			 AND type = 'Routine Maintenance']){
                       
            	 Case newCase = new Case();
                 newCase.Subject = 'Routine Maintenance';
                 newCase.Type =  'Routine Maintenance'; 
                 newCase.Status = 'New';
                 newCase.Vehicle__c = c.Vehicle__c;
				 newCase.Date_Reported__c = Date.today(); 
                 newCase.Date_Due__c  = (c.Work_Parts__r.size() > 0 ? Date.today().addDays(c.Work_Parts__r[0].Equipment__r.Maintenance_Cycle__c.intValue()) : null);
				 newCase.Equipment__c = c.Equipment__c;
                 newCase.ParentId = c.id;
                                     
                                     
				if(c.Work_Parts__r.Size()>0){
                
                    for(Work_Part__c workList: c.Work_Parts__r ){
                          allWorkParts.add(c.Work_Parts__r);
                    }	
                }                                     
                                     
                 listInsertCase.add(newCase);
      	}
	       
        if(listInsertCase.size() > 0){
            insert listInsertCase;
        }
         
    }        
   
}