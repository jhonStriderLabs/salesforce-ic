public with sharing class NewCaseListController {
  
    
    public List<Case> getNewCases() {
     List<Case> results = [SELECT Id, CaseNumber, Origin, Status, Account.Name FROM Case Where Status = 'New'];
     return results;
	}

}