public class QueryBuilderException extends SystemException {
    
    public QueryBuilderException(String message, List<String> args) {
        super(message, args);
    }

    public QueryBuilderException(String message, List<String> args, Exception e) {
        super(message, args, e);
    }
}