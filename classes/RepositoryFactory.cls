global class RepositoryFactory implements IRepositoryFactory {
    
    global IRepository create(SObjectType soType) {
        
        if (SObjectFields.defaultFields.containsKey(soType)) {
        	return new Repository(soType, SObjectFields.defaultFields.get(soType));
        }
        return null;
        //throw new CapCommunityCreateRepositoryException('Invalid repository type: {0}', new String[] { soType.getDescribe().getName() });
    }
}