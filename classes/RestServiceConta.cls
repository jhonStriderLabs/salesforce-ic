@RestResource(urlMapping='/account/v1.0/*')  
global class RestServiceConta {
    
    @HttpPost 
    global static void doPost(Contas contas){
    
        Log_de_integracao__c Log = new Log_de_integracao__c();
        
        List<Account> Accounts = contas.getAccountList();
        
        log.IntegratingName__c =  'account';
        log.request__c = XML.serialize(contas, 'Contas');
        
        List<Database.UpsertResult> results = Database.upsert(Accounts, Account.CNPJ__c, false);
        
        
   	  for(Integer i = 0; i < Accounts.size(); i++){
            Database.UpsertResult s = results[i];
            Account a = Accounts[i];
            Conta c = contas.conta[i];
            
            system.debug(c.CNPJ);
            
            if (!s.isSuccess()) {
                c.Resultado = new Resultado('Erro ao criar/atualizar registro.', true, null, s.getErrors());
                log.Error__c = true;
            }else{
                c.Resultado = new Resultado('Registro criado/atualizado com sucesso', false, a.Id);                
            }
        }
        
        String resposta = XML.serialize(contas, 'Contas');         
        log.Response__c = resposta;

        if (!Test.isRunningTest()){
            RestResponse res = RestContext.response;
            res.addHeader('Accept', 'application/xml');
            res.addHeader('Content-Type', 'application/xml; charset=UTF-8');
            res.responseBody = Blob.valueOf('<Response>' + resposta + '</Response>');    
        }
        
        INSERT log;  
        
    }
    
    global class Contas{
	
    global List<Conta> Conta = new List<Conta>();
                
        public List<Account> getAccountList(){
		
            list<Account> accounts = new list<Account>();
            
			for(Conta ct : conta){
    
                Account acc = new Account();
                
                
                
                acc.Name = ct.Name1;
                acc.CNPJ__c = ct.CNPJ;
                acc.BillingStreet = ct.BillingStreet;
                acc.BillingCity = ct.BillingCity;
                acc.BillingPostalCode = ct.BillingPostalCode;
                
                accounts.add(acc);
           }
            
            return accounts;            
        }
     
    }
    
    
    
    
    global class Conta{
        global String CNPJ;
        global String Name1;
        global String BillingStreet;
        global String BillingCity;
        global String BillingStateCode;
        global String BillingPostalCode;
        global String BillingCountryCode;
        global List<Contatos> Contatos;
        
        global Resultado Resultado;
    }
    
     global class Contatos{
	    global List<Contato> Contato = new List<Contato>();
     }
    
    global class Contato{
        global String firstnameTeste;
        global String LastNameTeste;
    }
   

global class Resultado{
    
        global String Message;
        global Boolean Error;
        global Id Id;
   		global List<Error> ErrorDetail = new List<Error>();
        
        public Resultado(String message, Boolean error, Id Id){
          this.Id = Id;    
            this.Message = message;
            this.error = error;
        }

        public Resultado(String message, Boolean error, Id Id, List<Database.Error> ListErrors){
          this.Id = Id;    
            this.Message = message;
            this.error = error;
            
            for(Database.Error er : ListErrors){
                ErrorDetail.add(new Error(String.ValueOf(er.getMessage()), String.ValueOf(er.getStatusCode()), String.ValueOf(er.getFields())));
            }
        }                
    }
    
    global class Error{
        global String Detail;
        global String StatusCode;
        global String FieldName;
        
        public Error(String Detail, String StatusCode, String FieldName){
            this.Detail = Detail;
            this.StatusCode = StatusCode;
            this.FieldName = FieldName;
        }
    }
}