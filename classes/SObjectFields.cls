// Field list for SObjects
public class SObjectFields {

	public static final Map<SObjectType, List<String>> defaultFields = new Map<SObjectType, List<String>>{
		User.sObjectType => new List<String>{'Id'},
		UserLogin.sObjectType => new List<String>{'UserId'},
		Account.sObjectType => new List<String>{'Id', 'Name'},
		Case.sObjectType => new List<String>{'Id','subject'},
		Group.sObjectType => new List<String>{'Id','Name'},
		Contact.sObjectType => new List<String>{'Id','Name'},
		Profile.sObjectType => new List<String>{'Id'},
		Attachment.sObjectType => new List<String>{'Id','Name'}
	};
}