@RestResource(urlMapping='/Conta/v1.0/*')  
global class ServiceConta {
    
    @HttpPost 
    global static void PostContas(Contas Contas){
        
        List<Account> AccountsTest = Contas.getListaContas();

        List<Database.UpsertResult> results = Database.upsert(AccountsTest, Account.id, false);
        
        system.debug(Results);
        
    }
    
    global class Contas{
  
    global List<Conta> Conta = new List<Conta>();
                
        public List<Account> getListaContas(){
        list<Account> accounts = new list<Account>();
        for(Conta ct : conta){
    
                Account acc = new Account();
            	acc.name = ct.RazaoSocial;
            	acc.CNPJ__c = ct.CNPJ;
                    
                accounts.add(acc);
           }
            
            return accounts;            
        }
     
    }


    public class Contatos {
        public List<Contato> Contato;
    }

    public class Contato {
        public String PrimeiroNome;
        public String SegundoNome;
    }

    public class Conta {
        public String RazaoSocial;
        public String CNPJ;
        public Contatos Contatos;
    }
}