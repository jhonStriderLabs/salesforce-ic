public with sharing class SingleRegisterAccountService {


    public static void init(){
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork( new Schema.SObjectType[]{Account.SObjectType, Contact.SObjectType});				

        List<Account> listAccount = new List<Account>();
        List<Contact> listContact = new List<Contact>();

        try {
       
            Account acc1 = new Account(Name = 'João Matheus 1');
            Account acc2 = new Account(Name = 'João Matheus 2');
            Contact cct1 = new Contact(AccountId = acc1.id, ChaveExterna__c = 150000, lastname = 'João Matheus');
            Contact cct2 = new Contact(AccountId = acc2.id, ChaveExterna__c = 11200, lastname = 'João Matheus 2');

            uow.registerUpsert(new List<Account>{acc1, acc2});
            uow.registerUpsert(new List<Contact>{cct1, cct2});
            uow.commitWork();
        
        } catch (Exception e) {

        } finally {
            
            List<RemoteResponse> response = new List<RemoteResponse>();
            

        }
    }

    public class RemoteResponse{

        public String ChaveExterna;
        public Integer Status;
        public String Message; 
        
        public RemoteResponse(){
            
        }
    }


}