public abstract class SystemException extends Exception {

    public SystemException(String message, List<String> args) {
        this(String.format(message, args));
    }

    public SystemException(String message, List<String> args, Exception e) {
        this(String.format(message, args), e);
    }
}