@isTest
private class TemperatureConverterTest {

    @isTest static void testWarmTemp(){
        decimal celsius = temperatureConverter.FahrenheitToCelsius(70);
        System.assertEquals(21.11,celsius);
    }
    
    @isTest static void testFreezingPoint(){
        decimal celsius = temperatureConverter.FahrenheitToCelsius(32);
        System.assertEquals(0,celsius);
    }
          @isTest static void testBoilingPoint(){
        decimal celsius = temperatureConverter.FahrenheitToCelsius(212);
        System.assertEquals(100,celsius,'Boiling point temperature is not expectecd.');
    }
    
          @isTest static void testNegativeTemp(){
        decimal celsius = temperatureConverter.FahrenheitToCelsius(-10);
        System.assertEquals(-23.33,celsius);
    }
    
}