@istest
private class TestVerifyDate {

    @istest static void method1crit1() {
        Date date1 = Date.parse('01/09/16');  //input any date1
        Date date2 = Date.parse('12/09/16');  // date2 
        Date dateexpected = Date.parse('12/09/16');  // expected test value
    
        Date date12 = VerifyDate.CheckDates(date1 , date2);
        System.assertEquals(dateexpected, date12); 
    }
    
    @istest static void method1crit2() {
        Date date1 = Date.parse('01/09/16');  //input any date1
		Date date2 = Date.parse('12/10/16');  // date2 
        Date dateexpected = Date.parse('30/09/16');  //introduce expected test value
    
        Date date12 = VerifyDate.CheckDates(date1 , date2);
        System.assertEquals(dateexpected, date12); 
    }
	
    @istest static void submethod1crit3() {
        Date date1 = Date.parse('01/09/16');  //input any date1
		Date date2 = Date.parse('12/11/16');  // date2 
        Date dateexpected = Date.parse('30/09/16');  //introduce expected test value
        Date date12 = VerifyDate.SetEndOfMonthDate(date1);
        System.assertEquals(dateexpected, date12);
    }
        
}