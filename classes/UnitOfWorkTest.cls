@isTest
public with sharing class UnitOfWorkTest {

@IsTest
static void challengeComplete(){
    

    fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
                            new Schema.SObjectType[] {
                                Account.SObjectType,
                                Contact.SObjectType,
                                Note.SObjectType
                            }
                        );
    Test.startTest();


        for (Integer i = 0; i < 100; i++) {
            Account acc = new Account(Name = 'Joao Matheus');
            uow.registerNew(acc);
            
            for (Integer h = 0; h < 5; h++) {
                Contact contato = new Contact(Firstname = 'Joao Matheus', LastName ='Pinheiro Matos');
                uow.registerNew(contato, Contact.AccountId, acc);
                Note noteSobject = new Note(Body = 'Joao Matheus',Title = 'SAMPLEEE');
                uow.registerNew(noteSobject, Note.ParentId, contato);
            }
        }
        
        
        
        uow.commitWork();

        System.assertEquals(500, [Select Id from Contact].size());
        System.assertEquals(100, [Select Id from Account].size());
        System.assertEquals(500, [Select Id from Note].size());


    Test.stopTest();
}

}