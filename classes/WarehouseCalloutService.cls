public with sharing class WarehouseCalloutService {

    private static final String WAREHOUSE_URL = 'https://th-superbadge-apex.herokuapp.com/equipment';
    
    //complete this method to make the callout (using @future) to the
    //REST endpoint and update equipment on hand.
    @Future (callout = true )
    public static void runWarehouseEquipmentSync(){
        
        List<Product2> equipList = new List<Product2>();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(WAREHOUSE_URL);
        req.setMethod('GET');
        Http http = new Http();
        HTTPResponse res = http.send(req);	
        String JSONContent = res.getBody().replaceAll('_id', 'equipmentId');
        List<Equipment> listEquipament = (List<Equipment>)System.JSON.deserialize(JSONContent, List<Equipment>.class);
        System.Debug(JSONContent);
        
        System.debug(listEquipament);
            
        for(Equipment Equip: listEquipament){
            
          equipList.add(new Product2(Lifespan_Months__c = Equip.lifespan,
                                     Cost__c = Equip.cost,
                                     Replacement_Part__c = true,
                                     Maintenance_Cycle__c = Equip.maintenancePeriod,
                                     Warehouse_SKU__c = Equip.sku,
                                     Current_Inventory__c = Equip.quantity,
                                     Name = Equip.name));  
        }
        
        Upsert equipList Warehouse_SKU__c; 
    }

    
    Public class Equipment{
        Public String  equipmentId;
        Public Boolean replacement;
        Public Integer quantity;
        Public String  name;
        Public Integer maintenanceperiod;
        Public Integer lifespan;
        Public Integer cost;
		Public String sku;
    }
}