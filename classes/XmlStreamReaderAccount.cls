public class XmlStreamReaderAccount {
 
    public class Account{
        string name;
        string email;
    }
    
    public Account[] parseAccounts(XmlStreamReader reader){
        
        Account[] accounts = new Account[0];
        boolean isSafeToGetNextXmlElement = true;
        
        while(isSafeToGetNextXmlElement){
            
            if(reader.getEventType() == XmlTag.START_ELEMENT)
            {
                if('Account' == reader.getLocalName()){
                    Account account = parseAccount(reader);
                    accounts.add(account);
                }
            }
            
            if(reader.hasNext()){
                reader.next();
            }else{
                isSafeToGetNextXmlElement = false;
                break;     
            }
        }
        return accounts;
    }
    
    Account parseAccount(XmlStreamReader reader){
        
        Account account = new Account();
        
        account.Name = reader.getAttributeValue(null,'name');
        account.email = reader.getAttributeValue(null, 'email');
        boolean isSafeToGetNextXmlElement = true;
        
        while(isSafeToGetNextXmlElement){
        
            if(reader.getEventType()==XmlTag.END_ELEMENT){
                break;
            }else if(reader.getEventType()==xmlTag.CHARACTERS){
                account.Name = reader.getText();      
                account.email= reader.getText();
            }
            if(reader.hasNext()){
                reader.next();
            }
            else {
            isSafeToGetNextXmlElement = false;
                break;
            }
         }
        return account;
	}
}