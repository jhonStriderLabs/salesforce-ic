@isTest
public class XmlStreamReaderAccountTest {

    	static testMethod void testAccountParser(){
        XmlStreamReaderAccount acc = new XmlStreamReaderAccount();
        
           String str = '<Account><name>João Matheus</name><email>joao.matos@hqsplus.com.br</email></Account>';
        
        XmlStreamReader reader = new XmlStreamReader(str);
        XmlStreamReaderAccount.Account[] accounts = acc.parseAccounts(reader);
        
        system.debug(accounts.size());
        
        for(XmlStreamReaderAccount.Account account : accounts)
        {
            system.debug(account);
            
        }
   } 
}