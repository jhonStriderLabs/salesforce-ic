public class XmlStreamReaderDemo {

    // Create a class Book for processing
    public class Book {
        String name;
        String author;
    }
    
    public class Contact{
        string firstname;
        string lastname;
    }
        public Contact[] parseContacts(XmlStreamReader reader) {
        Contact[] Contacts = new Contact[0];
        boolean isSafeToGetNextXmlElement = true;
      
        while(isSafeToGetNextXmlElement) {
            // Start at the beginning of the book and make sure that it is a book
            if (reader.getEventType() == XmlTag.START_ELEMENT) {
                if ('Contact' == reader.getLocalName()) {
                    // Pass the book to the parseBook method (below) 
                    Contact contact = parseContact(reader);
                    Contacts.add(contact);
                }
            }
            // Always use hasNext() before calling next() to confirm 
            // that we have not reached the end of the stream
            if (reader.hasNext()) {
                reader.next();
            } else {
                isSafeToGetNextXmlElement = false;
                break;
            }
        }
        return contacts;
    }

    // Parse through the XML, determine the author and the characters
    Contact parseContact(XmlStreamReader reader) {
        Contact contact = new Contact();
        contact.lastname = reader.getAttributeValue(null, 'lastname');
        boolean isSafeToGetNextXmlElement = true;
        while(isSafeToGetNextXmlElement) {
            if (reader.getEventType() == XmlTag.END_ELEMENT) {
                break;
            } else if (reader.getEventType() == XmlTag.CHARACTERS) {
                contact.firstname = reader.getText();
            }
            // Always use hasNext() before calling next() to confirm 
            // that we have not reached the end of the stream
            if (reader.hasNext()) {
                reader.next();
            } else {
                isSafeToGetNextXmlElement = false;
                break;
            }
        }
        return contact;
    }
}