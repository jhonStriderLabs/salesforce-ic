//@isTest
public class XmlStreamReaderDemoTest {
    // Test that the XML string contains specific values
    public static void testContactParser() {

        XmlStreamReaderDemo demo = new XmlStreamReaderDemo();

        String str = '<contacts><contact firstname="Chatty">Foo bar</contact>' +
            '<contact firstname="Sassy">Baz</contact></contacts>';

        XmlStreamReader reader = new XmlStreamReader(str);
        XmlStreamReaderDemo.Contact[] contacts = demo.parseContacts(reader);

        System.debug(contacts.size());

        for (XmlStreamReaderDemo.Contact contact : contacts) {
            System.debug(contact);
        }
    }
}