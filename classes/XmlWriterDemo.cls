global class XmlWriterDemo {

   public String GetDadosCase(Id CaseId){
		
        RMA RMA = new RMA();
        
        case s = [SELECT Id,CaseNumber, Account.id, Account.CNPJ__c FROM Case Where Id =: CaseId];
        
        RMA.IdCaseCRM = s.CaseNumber;
        RMA.CNPJCliente = s.Account.CNPJ__c;
        RMA.LoteDevolvido = '11111'; 
        RMA.MotivoDevolucao = 'Material Avariado';
        RMA.QuantidadeDevolvida = '123412';
        RMA.NumeroNF = '134123';
       
        System.debug('Numero do Caso : ' + RMA.IdCaseCRM + '/n CNPJ DO CLIENTE : ' + RMA.CNPJCliente + ' /n Numero do Lote Devolvido : ' + RMA.LoteDevolvido + '/n Motivo de Devolução : ' +  RMA.MotivoDevolucao);
         
        String XMLRMA = GeraXMLRMA(RMA.IdCaseCRM,RMA.CNPJCliente,RMA.LoteDevolvido,RMA.MotivoDevolucao,RMA.QuantidadeDevolvida,RMA.NumeroNF);
        Return XMLRMA;
    }
    
     
    public string GeraXMLRMA(String IdCase,String CNPJ, string Lote, string Motivo, String QtdDevolvida, String NumeroNF){
            
        	XmlStreamWriter W = new XmlStreamWriter();
            W.writeStartDocument ( null , '1.0 codificação = UTF-8' );
        	W.writeStartElement(null, 'RMA', null);
            W.writeStartElement(null,'IdCaseCRM',null);
			W.writeCharacters(IdCase);
			W.writeEndElement();
	 		W.writeStartElement(null,'CNPJCliente',null);
			W.writeCharacters(CNPJ);
			W.writeEndElement();
		    W.writeStartElement(null,'LoteDevolvido',null);
			W.writeCharacters(Lote);
			W.writeEndElement();
		    W.writeStartElement(null,'MotivoDevolucao',null);
			W.writeCharacters(Motivo);
			W.writeEndElement();
 			W.writeStartElement(null,'QuantidadeDevolvida',null);
			W.writeCharacters(QtdDevolvida);
			W.writeEndElement();
            W.writeStartElement(null,'NumeroNF',null);
        	W.writeCharacters(NumeroNF);
			W.writeEndElement();
            W.writeEndElement();
        	String xmlOutput = W.getXmlString();
            system.debug(xmlOutput);
            W.close();
        
       	   Return xmlOutput;
    }
    
    
    global class RMA{
    global String IdCaseCRM;
    global String CNPJCliente; 
    global String LoteDevolvido;
    global String MotivoDevolucao;
    global String QuantidadeDevolvida;
    global String NumeroNF;
        
    }    
}