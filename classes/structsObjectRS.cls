@RestResource(urlMapping='/structObjects/*')
global with sharing class structsObjectRS {

    @HttpPost 
    global static String getStruct(String objeto){
        String response = '';
        String[] types = new String[]{'Account'};
        
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
        
        for(Schema.DescribeSobjectResult res : results) {
            
            integer count = 0;
            for(Schema.SObjectField field : res.fields.getMap().values()){
                count++;
                Map<String, Schema.SObjectField> M = Schema.SObjectType.Account.fields.getMap();
                
                Schema.SObjectField field2 = M.get(String.ValueOf(field));
                Schema.DisplayType FldType = field2.getDescribe().getType();
                
                if(res.fields.getMap().values().size() == count){
                    response += String.ValueOf(field) + ' : ' + '!String ';
                }else{
                    response += String.ValueOf(field) + ' : ' + '!String ' ;
                }
            }
        }

        System.debug( '{' + response + '}');

        return  response;
    }    

}