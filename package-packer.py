import xml.etree.ElementTree as ET
import sys
import os 
import shutil 
from shutil import copyfile, copytree
import re

def namespace(element):
    m = re.match('\{.*\}', element.tag)
    return m.group(0) if m else ''

def getfile(type, member, files, actualStatus):
    
    configs = {
    'AuthProvider': [['authproviders','authprovider']],
    'CustomApplication': [['applications','app']],
    'AppMenu': [['appMenus','appMenu']],
    'ApprovalProcess': [['approvalProcesses','approvalProcess']],
    'AssignmentRules': [['assignmentRules','assignmentRules']],
    'AutoResponseRules': [['autoResponseRules','autoResponseRules']],
    'ApexClass': [['classes','cls'],['classes', 'cls-meta.xml']],
    'Community': [['communities','community']],
    'ApexComponent': [['components','component'], ['components', 'component-meta.xml']],
    'CustomApplicationComponent': [['customApplicationComponents','customApplicationComponent']],
    'DataCategoryGroup': [['datacategorygroups','datacategorygroup']],
    'EntitlementProcess': [['entitlementProcesses','entitlementProcess']],
    'EscalationRules': [['escalationRules','escalationRules']],
    'Fields': [['customFields']],
    'Flow': [['flows','flow']],
    'Group': [['groups','group']],
    'HomePageComponent': [['homePageComponents','homePageComponent']],
    'HomePageLayout': [['homePageLayouts','homePageLayout']],
   	'CustomLabel' : [['labels', 'labels']],
    'CustomLabels': [['labels','labels']],
    'Layout': [['layouts','layout']],
    'Letterhead': [['letterhead','letter']],
    'LiveChatAgentConfig': [['liveChatAgentConfigs','liveChatAgentConfig']],
    'LiveChatButton': [['liveChatButtons','liveChatButton']],
    'LiveChatDeployment': [['liveChatDeployments','liveChatDeployment']],
    'MilestoneType': [['milestoneTypes','milestoneType']],
    'Network': [['networks','network']],
    'CustomObject': [['objects','object']],
    'CustomObjectTranslation': [['objectTranslations','objectTranslation']],
    'ApexPage': [['pages','page'], ['pages', 'page-meta.xml']],
	'PathAssistant': [['pathAssistants','pathAssistant']],
    'PermissionSet': [['permissionsets','permissionset']],
    'Profile': [['profiles', 'profile']],
    'Queue': [['queues','queue']],
    'QuickAction': [['quickActions','quickAction']],
    'RemoteSiteSetting': [['remoteSiteSettings','remoteSite']],
    'ReportType': [['reportTypes','reportType']],
    'Role': [['roles','role']],
    'Skill': [['skills','skill']],
    'Settings': [['settings','settings']],
    'SiteDotCom': [['siteDotComSites','site'], ['siteDotComSites', 'site-meta.xml']],
    'CustomSite': [['sites','site']],
    'StaticResource': [['staticresources','resource'], ['staticresources', 'resource-meta.xml']],
    'CustomTab': [['tabs','tab']],
    'CustomMetadata': [['customMetadata','md']],
    'ApexTrigger': [['triggers','trigger'],['triggers','trigger-meta.xml']],
    'CustomPageWebLink': [['weblinks','weblink']],
    'Workflow': [['workflows','workflow']],
    'WorkflowAlert': [['workflows', 'workflow']],
    'WorkflowFieldUpdate': [['workflows', 'workflow']],
    'WorkflowTask': [['workflows', 'workflow']],
    'WorkflowRule': [['workflows', 'workflow']],
    'WebLink': [['weblinks', 'weblink']],
    'RemoteSiteSettings': [['remoteSiteSettings', 'remoteSite']],
    'Report': [['reports', 'report']],
    'Dashboard': [['dashboards', 'dashboard']],
    'Document': [['documents', '-meta.xml']],
    'EmailTemplate': [['email', 'email'], ['email', 'email-meta.xml']],
    'SharingCriteriaRule': [['sharingRules', 'sharingRules']],
    'SharingRules': [['sharingRules', 'sharingRules']],
    'GlobalPicklist': [['globalPicklists', 'globalPicklist']],
    'FlowDefinition': [['flowDefinitions', 'flowDefinition']],
    'FlexiPage': [['flexipages', 'flexipage']],
    'AuraDefinitionBundle': [['aura']],
    'ConnectedApp': [['connectedApps', 'connectedApp']],
    'GlobalValueSet': [['globalValueSets', 'globalValueSet']],
    'StandardValueSet': [['standardValueSets', 'standardValueSet']],
	'SharingSet': [['sharingSets', 'sharingSet']],
    'CallCenter': [['callCenters', 'callCenter']],
    'CustomPermission': [['customPermissions', 'customPermission']],
    'NamedCredential': [['namedCredentials', 'namedCredential']],
	'MatchingRule': [['matchingRules', 'matchingRule']],
	'DuplicateRule': [['duplicateRules', 'duplicateRule']],
	'ContentAsset': [['contentassets', 'asset'], ['contentassets', 'asset-meta.xml']],
	'WaveApplication': [['wave', 'wapp']],
	'WaveDashboard': [['wave', 'wdash']],
	'WaveDataflow': [['wave', 'wdf']],
	'WaveDataset': [['wave', 'wds']],
    'QueueRoutingConfig':[['queueRoutingConfigs','queueRoutingConfig']], 
    'LightningComponentBundle':[['lwc']]
    }
  
    if(type == 'LightningComponentBundle' or type == 'AuraDefinitionBundle'): 
      for config in configs[type]:
        foldersrc = config[0]
        newpath = os.path.join(sys.argv[2], foldersrc)
        if not os.path.exists(newpath):
          os.makedirs(newpath)
        src = os.path.join(sys.argv[3], foldersrc, member)
        des = os.path.join(sys.argv[2], foldersrc, member)
        copytree(src,des)
    elif(type=='Profile' or type == 'PermissionSet'):
        for config in configs[type]:
            foldersrc = config[0]
            extension = config[1]
            newpath = os.path.join(sys.argv[2], foldersrc)
            if not os.path.exists(newpath):
                os.makedirs(newpath)
            xmlProfile = ET.parse(os.path.join(sys.argv[3],foldersrc, member + "." + extension ))
            rootProfile = xmlProfile.getroot()
            for child in rootProfile:
                if(child.tag == str(namespace+"userPermissions")):
                    messageErro = type + " " + member + " :: Remova a tag userPermissions do metadado"
                    raise Exception(messageErro)
            src = os.path.join(sys.argv[3], foldersrc, member + "." + extension )
            des = os.path.join(sys.argv[2], foldersrc, member + "." + extension )
            copyfile(src,des)
    elif(type=='ConnectedApp'):
        for config in configs[type]:
            foldersrc = config[0]
            extension = config[1]
            newpath = os.path.join(sys.argv[2], foldersrc)
            if not os.path.exists(newpath):
                os.makedirs(newpath)
            xmlProfile = ET.parse(os.path.join(sys.argv[3],foldersrc, member + "." + extension ))
            rootProfile = xmlProfile.getroot()
            for child in rootProfile:
                if(child.tag == str(namespace+"oauthConfig")):
                    for m in child:
                        if(m.text == 'Full'):
                            messageErro = type + " " + member + " : Nao permitido conceder o acesso full ao Connect App"
                            raise Exception(messageErro)
            src = os.path.join(sys.argv[3], foldersrc, member + "." + extension )
            des = os.path.join(sys.argv[2], foldersrc, member + "." + extension )
            copyfile(src,des)
    elif(type=='ApexClass' or type=='ApexTrigger'):
        for config in configs[type]:
            foldersrc = config[0]
            extension = config[1]
            newpath = os.path.join(sys.argv[2], foldersrc)
            if not os.path.exists(newpath):
                os.makedirs(newpath)
            src = os.path.join(sys.argv[3], foldersrc, member + "." + extension)
            des = os.path.join(sys.argv[2], foldersrc, member + "." + extension)
            copyfile(src,des)
    else:
      for config in configs[type]:
        foldersrc = config[0]
        extension = config[1]
        newpath = os.path.join(sys.argv[2], foldersrc)
        if not os.path.exists(newpath):
          os.makedirs(newpath)

        src = os.path.join(sys.argv[3], foldersrc, member.decode("utf-8") + "." + extension)
        des = os.path.join(sys.argv[2], foldersrc, member.decode("utf-8") + "." + extension)
        copyfile(src,des)
    return files

try:
    xml = ET.parse(sys.argv[1])
except IOError:
    print('O arquivo package.xml nao foi encontrado no ambiente')

root = xml.getroot()
workspace = sys.argv[2]
newpath = sys.argv[2]
namespace = namespace(root)
files = [] 

if not os.path.exists(newpath):
    os.makedirs(newpath)
else:
    shutil.rmtree(newpath)
    os.makedirs(newpath)

src = os.path.join(sys.argv[1])
des = os.path.join(sys.argv[2],'package.xml')
copyfile(src,des)

for child in root: 
    if(child.tag == str(namespace + "types")):
        name = child.find(str(namespace + "name")).text
        for member in child:
            if(member.tag == str(namespace + "members")):
                files = getfile(name, member.text.encode('utf-8'), files, '')
                
print('Montagem Finalizada')






#python package-packer.py package.xml sfdx-project/force-app/main force-app/main 
