trigger ClosedOpportunityTrigger on Opportunity (after insert, before update) {

    List<Task> TaskList = new List<Task>();

    for(Opportunity opp: [Select id, StageName,ForecastCategoryName from Opportunity where Id in: Trigger.new and StageName =: 'Closed Won' ])
                   
          		    taskList.add(new Task(Subject='Follow Up Test Task', whatId=opp.Id));

    if(TaskList.size()>0){
        insert TaskList;
    }
    
}