trigger LeadTrigger on Lead (After insert) {

    
    if(Trigger.isInsert && Trigger.isAfter){
        LeadTriggerHelper.createTasks(Trigger.new);
    }
}