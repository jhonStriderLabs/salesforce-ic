trigger MaintenanceRequest on Case (before update, after update) {
    
    if(Trigger.isAfter || Trigger.isBefore){
      MaintenanceRequestHelper.updateWorkOrders(Trigger.OldMap, Trigger.NewMap);  
   }
}