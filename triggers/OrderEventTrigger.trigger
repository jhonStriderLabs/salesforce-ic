trigger OrderEventTrigger on Order_Event__e (after insert) {

    List<Task> taskList = new List<Task>();
    
    for(Order_Event__e e : Trigger.New){
        if(e.Has_Shipped__c){
        	taskList.add( new task(Priority =  'Medium', Subject = 'Follow up on shipped order ' + e.Order_Number__c , OwnerId = e.CreatedById));    
        }
    }
    
    if(taskList.size() > 0){
        
        insert taskList;
        system.Debug('TTTTT' + taskList);
    }
    
    
    
    
    
    
    
}