<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Custumer</fullName>
        <field>Type</field>
        <literalValue>Customer</literalValue>
        <name>Custumer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Propriet_rio_texas</fullName>
        <description>Descrição teste</description>
        <field>OwnerId</field>
        <lookupValue>jmpinheiro11@gmail.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Proprietário texas</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Prostct</fullName>
        <field>Type</field>
        <literalValue>Prospect</literalValue>
        <name>Prostct</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>pendentes</fullName>
        <field>Type</field>
        <literalValue>Pending</literalValue>
        <name>pendentes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Regra de teste</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.BillingState</field>
            <operation>equals</operation>
            <value>TX</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Propriet_rio_texas</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Acompanhar_nova_conta</name>
                <type>Task</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>Acompanhar_nova_conta</fullName>
        <assignedTo>jmpinheiro11@gmail.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Acompanhar nova conta.</subject>
    </tasks>
</Workflow>
