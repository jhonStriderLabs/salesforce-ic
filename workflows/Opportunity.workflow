<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Status_aprovado</fullName>
        <field>Status_do_percentual_de_desconto__c</field>
        <literalValue>Aprovado</literalValue>
        <name>Status aprovado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_n_o_aprovado</fullName>
        <field>Status_do_percentual_de_desconto__c</field>
        <literalValue>Não aprovado</literalValue>
        <name>Status não aprovado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
